#include <algorithm>
#include <chrono>
#include <fstream>
#include <iostream>
#include <regex>
#include <string>
#include <vector>
using namespace std;

struct Fabric
{
  int ID;
  int offsetLeft;
  int offsetTop;
  int width;
  int height;
};

bool ParseInput(string filePath, vector<Fabric> *contents)
{
  bool retval = false;

  if(contents == 0)
  {
    cout << "contents is null" << endl;
    retval = false;
  }
  else
  {
    ifstream file(filePath);
    string line;

    if(!file.is_open())
    {
      cout << "Could not open file located at " << filePath << endl;
      retval = false;
    }
    else
    {

      while(getline(file, line))
      {
        regex expr("^\\#(\\d+)\\s*@\\s*(\\d+),(\\d+):\\s*(\\d+)x(\\d+)$");
        smatch match;
        if(regex_search(line, match, expr))
        {
          // I know, but I do not know how to regex properly in C++
          Fabric f = {stoi(match[1].str().c_str()), 
                      stoi(match[2].str().c_str()), 
                      stoi(match[3].str().c_str()), 
                      stoi(match[4].str().c_str()), 
                      stoi(match[5].str().c_str())};
          contents->push_back(f);
        }
      }

      file.close();
      retval = true;
    }
  }

  return retval;
}

int CalculateOverlaps(vector<Fabric> *input)
{
  int sheet[1000][1000] = {0};
  int overlap = 0;

  for(Fabric f: *input)
  {
    for(int w = f.offsetLeft; w < f.offsetLeft + f.width; w++)
    {
      for(int h = f.offsetTop; h < f.offsetTop + f.height; h++)
      {
        sheet[w][h]++;
      }
    }
  }

  for(int h = 0; h < 1000; h++)
  {
    for(int w = 0; w < 1000; w++)
    {
      if(sheet[w][h] > 1)
      {
        overlap++;
      }
    }
  }

  return overlap;
}

int CalculateFreeFabric(vector<Fabric> *input)
{
  int sheet[1000][1000] = {0};
  vector<int> freefabrics;

  for(Fabric f: *input)
  {
    bool isOverlapped = false;
    for(int w = f.offsetLeft; w < f.offsetLeft + f.width; w++)
    {
      for(int h = f.offsetTop; h < f.offsetTop + f.height; h++)
      {
        if(sheet[w][h] > 0)
        {
          isOverlapped = true;
          freefabrics.erase(std::remove(freefabrics.begin(), freefabrics.end(), sheet[w][h]), freefabrics.end());
        }

        sheet[w][h] = f.ID;
      }
    }

    if(!isOverlapped)
    {
      freefabrics.push_back(f.ID);
    }
  }

  return freefabrics[0];
}

int main(int argc, char *argv[])
{
  vector<Fabric> parsedInput;
  int result = 0;
  bool success = false;

  if(argc != 2)
  {
    cout << "Please use the following syntax: ./InventoryManagementSystem <input file>" << endl;
  }
  else
  {    
    ParseInput(argv[1], &parsedInput);

    cout << "Total square inches of overlap is: " << CalculateOverlaps(&parsedInput) << endl;

    cout << "Free fabric is: " << CalculateFreeFabric(&parsedInput) << endl;
  }
}