#include <algorithm>
#include <chrono>
#include <fstream>
#include <iostream>
#include <map>
#include <string>
#include <unordered_set>
using namespace std;


bool ParseInput(string filePath, unordered_set<string> *contents)
{
  bool retval = false;

  if(contents == 0)
  {
    cout << "contents is null" << endl;
    retval = false;
  }
  else
  {
    ifstream file(filePath);
    string line;

    if(!file.is_open())
    {
      cout << "Could not open file located at " << filePath << endl;
      retval = false;
    }
    else
    {

      while(getline(file, line))
      {
        contents->insert(line);
      }

      file.close();
      retval = true;
    }
  }

  return retval;
}

bool part1(unordered_set<string> *input, int *result)
{
  int amountTwice = 0, amountThrice = 0;

  for(const auto& elem: *input)
  {
    map<char, int> charFrequency;

    // Count amount of characters in each ID
    for(const char& c: elem)
    {
      pair<map<char, int>::iterator, bool> ret;
      ret = charFrequency.insert(pair<char, int>(c, 1));
      if(ret.second == false)
      {
        charFrequency[c]++;
      }
    }
    
    // Count the amount of twice and thrice characters
    bool twice = false, thrice = false;
    for(const auto& i: charFrequency)
    {
      // Only use it once
      if(i.second == 2 && !twice)
      {
        amountTwice++;
        twice = true;
      }
      else if(i.second == 3 && !thrice)
      {
        amountThrice++;
        thrice = true;
      }
    }
  }

  *result = amountTwice * amountThrice;

  cout << "Twice: " << amountTwice << endl;
  cout << "Thrice: " << amountThrice << endl;
  cout << "Checksum: " << *result << endl;

  return true;
}

bool part2(unordered_set<string> *input, string *result)
{
  for(auto IDa : *input)
  {
    for(auto IDb : *input)
    {
      int difference = 0;
      auto iterIDa = begin(IDa);
      auto iterIDb = begin(IDb);
      *result = "";

      for(;iterIDa != end(IDa); ++iterIDa, ++iterIDb)
      {
        if(*iterIDa != *iterIDb)
        {
          difference++;
        }
        else
        {
          *result += *iterIDa;
        }
      }

      if(difference == 1)
      {
        cout << "The IDs " << IDa << " and " << IDb << " have 1 difference." << endl;
        return true;
      }
    }
  }

  return false;
}

int main(int argc, char *argv[])
{
  unordered_set<string> parsedInput;
  int result = 0;
  auto timeBegin = chrono::high_resolution_clock::now()
  auto timeEnd = chrono::high_resolution_clock::now();
  std::chrono::duration<double> timeElapsed;
  bool success = false;

  if(argc != 2)
  {
    cout << "Please use the following syntax: ./InventoryManagementSystem <input file>" << endl;
  }
  else
  {    
    ParseInput(argv[1], &parsedInput);

    timeBegin = chrono::high_resolution_clock::now();
    success = part1(&parsedInput, &result);
    timeEnd = chrono::high_resolution_clock::now();
    timeElapsed = timeEnd - timeBegin;

    if(success)
    {
      cout << "Result of part 1: " <<  result<< endl;
      cout << "Duration: " << timeElapsed.count()*1000 << " ms" << endl;
    }

    cout << endl;
    string part2Result;
    timeBegin = chrono::high_resolution_clock::now();
    success = part2(&parsedInput, &part2Result);
    timeEnd = chrono::high_resolution_clock::now();
    timeElapsed = timeEnd - timeBegin;
    if(success)
    {
      cout << "Result of part 2: " << part2Result<< endl;
      cout << "Duration: " << timeElapsed.count()*1000 << " ms" << endl;
    }
  }
}