#include <algorithm>
#include <fstream>
#include <iostream>
#include <string>
#include <unordered_set>
#include <vector>
using namespace std;


bool ParseInput(string filePath, vector<int> *contents)
{
  bool retval = false;

  if(contents == 0)
  {
    cout << "contents is null" << endl;
    retval = false;
  }
  else
  {
    ifstream file(filePath);
    string line;

    if(!file.is_open())
    {
      cout << "Could not open file located at " << filePath << endl;
      retval = false;
    }
    else
    {

      while(getline(file, line))
      {
        contents->push_back(stoi(line, nullptr));
      }

      file.close();
      retval = true;
    }
  }

  return retval;
}

int part1(vector<int> *input)
{
  int result = 0;

  for(vector<int>::iterator it = input->begin(); it != input->end(); ++it)
  {
    result += *it;
  }

  return result;
}

int part2(vector<int> *input)
{
  unordered_set<int> reachedFrequencies;
  int result = 0;

  //for(vector<int>::iterator it = input->begin(); it != input->end(); ++it)
  for(size_t i = 0; i <= input->size(); i++)
  {
    if(i == input->size())
    {
      i = 0;
    }
    
    result += input->at(i);
    if(reachedFrequencies.find(result) != reachedFrequencies.end())
    {
      break;
    }
    else
    {
      reachedFrequencies.insert(result);
    }

  }
  return result;
}

int main(int argc, char *argv[])
{
  vector<int> parsedInput;
  int result = 0;

  if(argc != 2)
  {
    cout << "Please use the following syntax: ./ChronalCalibration <input file>" << endl;
  }
  else
  {    
    ParseInput(argv[1], &parsedInput);

    // parsedInput = {1,-1};
    // cout << "Result of part 2: " << part2(&parsedInput) << endl;

    cout << "Result of part 1: " << part1(&parsedInput) << endl;
    cout << "Result of part 2: " << part2(&parsedInput) << endl;
  }
}